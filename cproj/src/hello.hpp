/* -*- C -*- ****************************************************************/

#ifndef __HELLO_H
#define __HELLO_H

#include <iostream>
#include <cstdio>
#include <conio.h>
#include <graph.h>
#include <string>
#include <fstream>

#include "serial.hpp"
#include "video.hpp"
#include "mouse.hpp"
#include "timer.hpp"
#include "window\wndmgr.hpp"

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

int MainWindowProc(Window *window, Event *event);

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif
#endif /* __HELLO_H */
