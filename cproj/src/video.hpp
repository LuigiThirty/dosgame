// -!- c++ -!- //////////////////////////////////////////////////////////////*
#ifndef __VIDEO_HPP
#define __VIDEO_HPP
#pragma once

#include <cstring>
#include <iostream>
#include <graph.h>

#include "types.hpp"

#define VGA_WIDTH  320
#define VGA_HEIGHT 200

enum VGA_BUFFER {
    VGA_BUFFER1,
    VGA_BUFFER2
};

class VideoHelper
{
    private:
    // VGA memory base pointer.
    UBYTE *VGABase;

    // Two 320x200x8 buffers for double buffering.
    UBYTE *buffer1;
    UBYTE *buffer2;

    enum VGA_BUFFER visible_buffer;

    public:
    VideoHelper()
    {
        VGABase = (UBYTE *)0xA0000;

        // Create two VGA pixel buffers.
        //buffer1 = new UBYTE[VGA_HEIGHT * VGA_WIDTH];
        //buffer2 = new UBYTE[VGA_HEIGHT * VGA_WIDTH];

        buffer1 = VGABase;
        buffer2 = VGABase;

        std::memset(buffer1, 0, VGA_HEIGHT * VGA_WIDTH);
        std::memset(buffer2, 0, VGA_HEIGHT * VGA_WIDTH);

        // Buffer 1 is the visible buffer at the start
        // we'll assume.
        visible_buffer = VGA_BUFFER1;
    }

    ~VideoHelper()
    {
        delete[] buffer1;
        delete[] buffer2;
    }

    static void SetMode13h();
    static void SetOriginalMode();

    void SwapBuffers();
    void EraseBuffer(VGA_BUFFER buffer);
    VGA_BUFFER GetVisibleBuffer();
    VGA_BUFFER GetBackBuffer();

    UBYTE *GetVisibleBufferPtr();
    UBYTE *GetBackBufferPtr();
};

// singleton, todo
extern VideoHelper videoHelper;

#endif // __VIDEO_HPP

