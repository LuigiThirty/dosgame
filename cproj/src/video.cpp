// -!- C++ -!- //////////////////////////////////////////////////////////////*

#include "video.hpp"

VideoHelper videoHelper;

static void VideoHelper::SetMode13h()
{
    _setvideomode(_MRES256COLOR);
}

static void VideoHelper::SetOriginalMode()
{
    _setvideomode(_DEFAULTMODE);
}

// Determine the visible buffer and copy the back buffer
// into VGABase. The back buffer is now the visible buffer.
void VideoHelper::SwapBuffers()
{
    switch(visible_buffer)
    {
        case VGA_BUFFER1:
            // Copy buffer2 to the screen.
            std::memcpy(VGABase, buffer2, VGA_HEIGHT*VGA_WIDTH);
            visible_buffer = VGA_BUFFER2;
            break;
        case VGA_BUFFER2:
            // Copy buffer1 to the screen.
            std::memcpy(VGABase, buffer1, VGA_HEIGHT*VGA_WIDTH);
            visible_buffer = VGA_BUFFER1;
            break;
        default:
            // ???
            break;
    }
}

void VideoHelper::EraseBuffer(VGA_BUFFER buffer)
{
    switch(buffer)
    {
        case VGA_BUFFER1:
            std::memset(buffer1, 0, VGA_HEIGHT*VGA_WIDTH);
            break;
        case VGA_BUFFER2:
            std::memset(buffer2, 0, VGA_HEIGHT*VGA_WIDTH);
            break;
        default:
            // ???
            break;
    }
}

VGA_BUFFER VideoHelper::GetVisibleBuffer()
{
    return visible_buffer;
}

VGA_BUFFER VideoHelper::GetBackBuffer()
{
    switch (visible_buffer)
    {
        case VGA_BUFFER1:
            return VGA_BUFFER2;
        case VGA_BUFFER2:
            return VGA_BUFFER1;
        default:
            // ???
            return VGA_BUFFER1;
    }
}

UBYTE *VideoHelper::GetVisibleBufferPtr()
{
    switch (visible_buffer)
    {
        case VGA_BUFFER1:
            return buffer1;
        case VGA_BUFFER2:
            return buffer2;
        default:
            return buffer1;
    }
}

UBYTE *VideoHelper::GetBackBufferPtr()
{
    switch (visible_buffer)
    {
        case VGA_BUFFER1:
            return buffer2;
        case VGA_BUFFER2:
            return buffer1;
        default:
            return buffer1;
    }
}