#ifndef __MOUSE_HPP
#define __MOUSE_HPP

#include <i86.h>
#include <cstdio>

#include "types.hpp"
#include "window\point.hpp"

struct MouseStatus {
    Pixel position;
    UBYTE buttons;
};

class Mouse
{
    public:
    static void Reset();
    static void Enable();
    static void Disable();
    static struct MouseStatus UpdateStatus();
};

#endif