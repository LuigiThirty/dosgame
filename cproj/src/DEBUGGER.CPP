#include "debugger.hpp"

Debugger g_Debugger;

char d_temp[256] = "";

static char banner[] = "*** Luigi Thirty's Fancy VT100 Debug Console ***\n";
static char f_keys[] = "F1=SPLIT F2=CONSOLE F3=WNDMGR\n";

void Debugger::SetupScroller(UBYTE scrollAreaStart)
{
    // Scroll the bottom half of the screen
    g_DebugConsole.WriteVT100(VT100_SETWIN2(25,48));

    // Banner
    g_DebugConsole.WriteVT100(VT100_HVPOS(24,0));

    for(int i=0; i<132; i++)
        d_temp[i] = '*';

    d_temp[132] = '\0';
    g_DebugConsole.Write(d_temp);

    // And move our cursor to the scroll window
    g_DebugConsole.WriteVT100(VT100_HVPOS(25,0));
}

void Debugger::SwitchScreen()
{
    // Switch to the active screen according to
    // the state variable.

    switch(state)
    {
    case DBG_SCREEN_F1:
        DrawF1Screen();
        break;
    case DBG_SCREEN_F2:
        DrawF2Screen();
        break;
    case DBG_SCREEN_F3:
        DrawF3Screen();
        break;
    default:
        DrawF1Screen();
        break;
    };
}

void Debugger::DrawBanner()
{
    g_DebugConsole.WriteVT100(VT100_SAVECURSOR);

    g_DebugConsole.WriteVT100(VT100_HVPOS(0,0));
    g_DebugConsole.Write(banner);

    g_DebugConsole.WriteVT100(VT100_RESTORECURSOR);
}

void Debugger::DrawFKeys()
{
    g_DebugConsole.WriteVT100(VT100_SAVECURSOR);

    g_DebugConsole.WriteVT100(VT100_HVPOS(1,0));
    g_DebugConsole.Write(f_keys);

    g_DebugConsole.WriteVT100(VT100_RESTORECURSOR);
}

// F1 is the split screen between the console and
// the info at the top, whatever that is.
void Debugger::DrawF1Screen()
{
    //DrawBanner();
    DrawFKeys();

    SetupScroller(10);
}

void Debugger::DrawF2Screen()
{
    DrawBanner();
    DrawFKeys();

    g_DebugConsole.Write("Not Implemented\n");
}

void Debugger::DrawF3Screen()
{
    DrawBanner();
    DrawFKeys();

    g_DebugConsole.Write("Not Implemented\n");
}
