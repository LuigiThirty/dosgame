#include "screen.hpp"

/* Rect */


UWORD ClickZone::newID = 1;

ClickZone::ClickZone()
    :ID(newID++)
{
}

UWORD ClickZone::GetID()
{
    return ID;
}

/* Screen */
Screen::Screen()
{
    foreColor = 0;
    offset = Point(0,0);
}

Rect Screen::GetRegionRect()
{
    Rect r;
    r.x = offset.x;
    r.y = offset.y;
    r.w = clipping_size.x;
    r.h = clipping_size.y;
    return r;
}

void Screen::AddClickZone(UWORD x, UWORD y, UWORD w, UWORD h)
{
    x += this->offset.x;
    y += this->offset.y;

    ClickZone zone;
    zone.boundRect.x = x;
    zone.boundRect.y = y;
    zone.boundRect.w = w;
    zone.boundRect.h = h;

    clickZones.push_back(zone);
}

UWORD Screen::CheckForClickZone(struct Point clickLocation)
{
    // If the pixel is inside a click zone, returns the
    // click zone's ID.

    clickLocation.x = clickLocation.x >> 1;

    for(std::vector<ClickZone>::iterator zone = clickZones.begin();
              zone != clickZones.end();
           ++zone)
    {
        if(zone->boundRect.ContainsPoint(&clickLocation))
        {
            return zone->GetID();
        }
    }

    // Return 0 if not inside a click zone.
    return 0;
}

void Screen::SetForeColor(UBYTE c)
{
    foreColor = c;
}

void Screen::PutRect(UWORD x, UWORD y, UWORD w, UWORD h)
{
    x += this->offset.x;
    y += this->offset.y;

    UWORD topOffset = (y * 320) + x;
    UWORD botOffset = ((y+h-1) * 320) + x;
    UWORD leftOffset = topOffset;
    UWORD rightOffset = topOffset + w-1;

    UBYTE *fb = videoHelper.GetBackBufferPtr();

    for(int i=0; i<w; i++)
    {
        fb[topOffset++] = foreColor;
        fb[botOffset++] = foreColor;
    }

    for(int i=0; i<h; i++)
    {
        fb[leftOffset] = foreColor;
        fb[rightOffset] = foreColor;

        leftOffset += VGA_WIDTH;
        rightOffset += VGA_WIDTH;
    }
}

void Screen::PutString(Font f, char *str, Point p)
{
    p += this->offset;

    int length = std::strlen(str);

    for(int i=0; i<length; i++)
    {
        p.x = p.x + PutCharacter(f, str[i], p, false) + 2;
    }
}

UBYTE Screen::PutCharacter(Font f, UBYTE c, Point p, bool apply_offset = true)
{
    // This can be called from PutString so we may
    // need to bypass the offset code.
    if(apply_offset)
        p += this->offset;

    // Put a character from the font F at point P.
    // Returns the character width.

    UWORD offset = (p.y * VGA_WIDTH) + p.x;
    UBYTE *fb = videoHelper.GetBackBufferPtr();

    FontGlyph glyph = f.GetGlyph(c);
    bool cellBlank = false;
    int charWidth = 0;

    for(int col = 0; col < f.GetSizeY(); col++)
    {
        for(int row = 0; row < f.GetSizeX(); row++)
        {
            if((glyph[col] & (1 << row)) > 0)
            {
                fb[offset + (row * VGA_WIDTH) + col] = 15;
                if(col > charWidth)
                    charWidth = col;
            }

            // 2 blank columns in a row means the character is over.
            else if(glyph[col] == 0)
            {
                if(cellBlank && c != 0x20)
                {
                    cellBlank = false;
                    continue;
                }
                else
                    cellBlank = true;
            }
        }
    }

    return charWidth;
}

/* TestScreen */
void TestScreen::Redraw()
{

}