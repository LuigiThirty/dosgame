#ifndef SERIAL_HPP
#define SERIAL_HPP

#include <conio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>

#include "vt100.hpp"

#define COM1_BASE   0x3F8
#define SER_THB     COM1_BASE+0x0
#define SER_DLL     COM1_BASE+0x0
#define SER_DLH     COM1_BASE+0x1
#define SER_LCR     COM1_BASE+0x4

class Serial
{
    private:
    std::ofstream DOSFile;

    public:
    Serial()
    {
        // Open a 19200 8N1 COM1
        outp(COM1_BASE+SER_LCR, _inp(COM1_BASE+SER_LCR) | 0x80);
        outp(COM1_BASE+SER_DLH, 0x00);
        outp(COM1_BASE+SER_DLL, 0x06);
        outp(COM1_BASE+SER_LCR, _inp(COM1_BASE+SER_LCR) & 0x7F);
        outp(COM1_BASE+SER_LCR, 0x03);
        DOSFile.open("COM1");

        SetupVT100();
    }

    void SetupVT100()
    {
        WriteVT100(VT100_CLEARSCREEN);

        // Enable line wrap, 132-column mode
        WriteVT100(VT100_SETWRAP);
        WriteVT100(VT100_SETCOL);

        WriteVT100(VT100_HVPOS(24,0));
    }

    void Write(char *c)
    {
        DOSFile << c;
        DOSFile.flush();
    }

    char vt_buf[10];
    void WriteVT100(char *code)
    {
        std::sprintf(vt_buf, "%c%s", ASCII_ESC, code);
        DOSFile << vt_buf;
        DOSFile.flush();
    }
};

extern Serial g_DebugConsole;
extern char g_DebugBuffer[256];

#define DCONSOLE_PRINT g_DebugConsole.Write(g_DebugBuffer);
#endif