#include "mouse.hpp"

void Mouse::Reset()
{
    union REGS r;

    r.h.ah = 0;
    r.h.al = 0;
    int386(0x33, &r, &r);
}

void Mouse::Enable()
{
    union REGS r;

    r.w.ax = 1;
    int386(0x33, &r, &r);
}

void Mouse::Disable()
{
    union REGS r;

    r.w.ax = 2;
    int386(0x33, &r, &r);
}

struct MouseStatus Mouse::UpdateStatus()
{
    struct MouseStatus mouseStatus;
    union REGS r;
    r.w.ax = 3;

    int386(0x33, &r, &r);

    mouseStatus.position.x = r.w.cx;
    mouseStatus.position.y = r.w.dx;
    mouseStatus.buttons = r.h.bl;

    return mouseStatus;
}