#ifndef TYPES_HPP
#define TYPES_HPP

#include <cstdint>

typedef unsigned char   UBYTE;
typedef unsigned short  UWORD;
typedef unsigned long   ULONG;

typedef char            BYTE;
typedef short           WORD;
typedef long            LONG;

typedef void *          PTR;

#define _S(STR) std::string(STR)

#endif