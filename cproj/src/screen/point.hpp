#ifndef POINT_HPP
#define POINT_HPP

#include "types.hpp"

struct Point {
    UWORD x, y;

    Point() { x = 0; y = 0; }
    Point(UWORD _x, UWORD _y) { x = _x; y = _y; }

    // Point + and - operators. Act as add/subtracting offsets.
    Point &operator += (const Point &p2)
    {
        this->x += p2.x;
        this->y += p2.y;
        return *this;
    }

    Point &operator -= (const Point &p2)
    {
        this->x -= p2.x;
        this->y -= p2.y;
        return *this;
    }

};

inline Point operator + (Point p1, const Point &p2)
{
    p1 += p2;
    return p1;
}

inline Point operator - (Point p1, const Point &p2)
{
    p1 -= p2;
    return p1;
}

typedef struct Point Pixel;

#endif