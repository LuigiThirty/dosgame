#ifndef CLIKZONE_HPP
#define CLIKZONE_HPP

#include "types.hpp"
#include "screen\rect.hpp"

class ClickZone
{
    static UWORD newID;
    const UWORD ID;

    public:
    struct Rect boundRect;
    ClickZone();

    UWORD GetID();
};

#endif
