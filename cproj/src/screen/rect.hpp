#ifndef RECT_HPP
#define RECT_HPP

#include "types.hpp"

struct Rect
{
    UWORD x, y, w, h;

    bool ContainsPoint(struct Point *p);
};

#endif