#include "screen\text\font.hpp"

#include <cstdio>

Font::Font(enum FontID f, UBYTE _sizeX, UBYTE _sizeY)
{
    switch(f)
    {
        case FONT_BMPLAIN:
        bitmap = (const UBYTE **)BMplain_font;
        break;

        default:
        bitmap = (const UBYTE **)BMplain_font;
        break;
    }

    sizeX  = _sizeX;
    sizeY  = _sizeY;
}

UBYTE Font::GetSizeX() { return sizeX; }
UBYTE Font::GetSizeY() { return sizeY; }

FontGlyph Font::GetGlyph(UBYTE number)
{
    // Returns a pointer to the glyph with ID [number].

    // Fonts start at 0x20 so subtract 0x20 from the number.
    number -= 0x20;

    return (FontGlyph)((ULONG)bitmap + (sizeY * number));
}