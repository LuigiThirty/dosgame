#ifndef FONT_HPP
#define FONT_HPP

#include "types.hpp"
#include "bmfonts\BMplain_font.h"

enum FontID {
     FONT_BMPLAIN,
};

typedef const UBYTE * FontGlyph;

class Font
{
    private:
    const UBYTE **bitmap;
    UBYTE sizeX, sizeY;

    public:
    Font(enum FontID, UBYTE _sizeX, UBYTE _sizeY);
    UBYTE GetSizeX();
    UBYTE GetSizeY();

    FontGlyph GetGlyph(UBYTE number);
};

#endif