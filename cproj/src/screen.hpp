#ifndef SCREEN_HPP
#define SCREEN_HPP

#include <iostream>
#include <vector>

#include "types.hpp"
#include "video.hpp"

#include "screen\clikzone.hpp"
#include "screen\point.hpp"
#include "screen\rect.hpp"
#include "screen\text\text.hpp"

class Screen
{
    private:
    UBYTE foreColor;
    std::vector<ClickZone> clickZones;

    // Offset of all coordinate operations from (0,0).
    // Effectively, the top-left coordinate of the screen.
    Point offset;

    // The clipping area of the window on the screen.
    // Combined with offset to produce the window size.
    Point clipping_size;

    public:
    Screen();

    virtual void Redraw() = 0;

    void SetOffset(Point p) { offset = p; }
    Point GetOffset() { return offset; }

    void SetClipSize(Point p) { clipping_size = p; }
    Point GetClipSize() { return clipping_size; }

    // Return the rectangle represented by offset and
    // clipping size. This is the window's draw region.
    Rect GetRegionRect();

    // Click zones.
    void AddClickZone(UWORD x, UWORD y, UWORD w, UWORD h);
    UWORD CheckForClickZone(struct Point clickLocation);

    // Primitive drawing.
    void SetForeColor(UBYTE c);
    void PutRect(UWORD x, UWORD y, UWORD w, UWORD h);

    // Text drawing.
    UBYTE PutCharacter(Font f, UBYTE c, Point p, bool apply_offset);
    void PutString(Font f, char *str, Point p);
};

class TestScreen: public Screen
{
    private:

    public:
    void Redraw();
};

#endif
