#ifndef TIMER_HPP
#define TIMER_HPP

#include <dos.h>
#include <cstdio>
#include <conio.h>

extern volatile int timer_ticks;
extern void (__interrupt __far *OldTimerISR)();
void __interrupt __far NewTimerISR();

void TimerInstallInterrupt();
void TimerRemoveInterrupt();

#endif