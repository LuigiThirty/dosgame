#include "timer.hpp"

extern volatile int timer_ticks = 0;
extern void (__interrupt __far *OldTimerISR)() = NULL;

void __interrupt __far NewTimerISR(void)
{
    timer_ticks++;

    // Chain to the original timer ISR.
    OldTimerISR();
}

void TimerInstallInterrupt()
{
    OldTimerISR = _dos_getvect(0x1C);
    _dos_setvect(0x1C, NewTimerISR);
}

void TimerRemoveInterrupt()
{
    _dos_setvect(0x1C, OldTimerISR);
}